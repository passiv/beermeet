package com.beermeet;

import android.content.Context;

import com.beermeet.facebook.User;
import com.beermeet.facebook.UserRequestTask;
import com.beermeet.ui.home.News;
import com.beermeet.ui.home.NewsContent;
import com.beermeet.ui.home.PartyPictures;
import com.beermeet.ui.message.MessageThread;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by passiv on 07/03/16.
 */
public class Cache {

    public static User user;
    private static News news;

    public static User getUser() {
        try {
            if (Cache.user == null) {
                return new UserRequestTask() {

                    @Override
                    protected void onPostExecute(User result) {
                        super.onPostExecute(result);
                        Cache.user = result;
                    }

                }.execute().get();
            } else {
                return Cache.user;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

//    public static User getUser(final Context context){
//        return new User(){
//            @Override
//            public Bitmap getProfilePicture() {
//                String path ="android.resource://com.beermeet/" + R.raw.habera_palo;
//                Bitmap bitmap = BitmapFactory.decodeFile(path); //wrong path
//                return bitmap;
//            }
//
//            @Override
//            public Date getBirthday() {
//                String date="11-June-07";
//                DateFormat formatter = new SimpleDateFormat("dd-MMM-yy");
//                try {
//                    return formatter.parse(date);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                return null;
//            }
//
//            @Override
//            public String getFullName() {
//                return "Palo Kabela";
//            }
//
//            @Override
//            public String getFirstName() {
//                return "Palo";
//            }
//
//        };
//    }

    public static List<MessageThread> getLatestMessageThreads() {
        List<MessageThread> threads = new ArrayList<>();
        threads.add(new MessageThread(1, getUser().getProfilePicture(), "Palo Habera", "This is a last message from Palo Habera", new Date()));
        threads.add(new MessageThread(2, getUser().getProfilePicture(), "Kurvela Para", "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean et est a dui semper facilisis. Pellentesque placerat elit a nunc. Nullam tortor odio, rutrum quis, egestas ut, posuere sed, felis. Vestibulum placerat feugiat nisl. Suspendisse lacinia, odio non feugiat vestibulum, sem erat blandit metus, ac nonummy magna odio pharetra felis. Vivamus vehicula velit non metus faucibus auctor. Nam sed augue. Donec orci. Cras eget diam et dolor dapibus sollicitudin. In lacinia, tellus vitae laoreet ultrices, lectus ligula dictum dui, eget condimentum velit dui vitae ante. Nulla nonummy augue nec pede. Pellentesque ut nulla. " +
                "Donec at libero. Pellentesque at nisl ac nisi fermentum viverra. Praesent odio. Phasellus tincidunt diam", new Date()));
        threads.add(new MessageThread(3, getUser().getProfilePicture(), "Bacov Kokot", "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean et est a dui semper facilisis. Pellentesque placerat elit a nunc. Nullam tortor odio, rutrum quis, egestas ut, posuere sed, felis. Vestibulum placerat feugiat nisl. Suspendisse lacinia, odio non feugiat vestibulum, sem erat blandit metus, ac nonummy magna odio pharetra felis. Vivamus vehicula velit non metus faucibus auctor. Nam sed augue. Donec orci. Cras eget diam et dolor dapibus sollicitudin. In lacinia, tellus vitae laoreet ultrices, lectus ligula dictum dui, eget condimentum velit dui vitae ante. Nulla nonummy augue nec pede. Pellentesque ut nulla. " +
                "Donec at libero. Pellentesque at nisl ac nisi fermentum viverra. Praesent odio. Phasellus tincidunt diam", new Date()));
        threads.add(new MessageThread(4, getUser().getProfilePicture(), "Vaso Patejdl", "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean et est a dui semper facilisis. Pellentesque placerat elit a nunc. Nullam tortor odio, rutrum quis, egestas ut, posuere sed, felis. Vestibulum placerat feugiat nisl. Suspendisse lacinia, odio non feugiat vestibulum, sem erat blandit metus, ac nonummy magna odio pharetra felis. Vivamus vehicula velit non metus faucibus auctor. Nam sed augue. Donec orci. Cras eget diam et dolor dapibus sollicitudin. In lacinia, tellus vitae laoreet ultrices, lectus ligula dictum dui, eget condimentum velit dui vitae ante. Nulla nonummy augue nec pede. Pellentesque ut nulla. " +
                "Donec at libero. Pellentesque at nisl ac nisi fermentum viverra. Praesent odio. Phasellus tincidunt diam", new Date()));
        threads.add(new MessageThread(5, getUser().getProfilePicture(), "Jim Carrey", "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean et est a dui semper facilisis. Pellentesque placerat elit a nunc. Nullam tortor odio, rutrum quis, egestas ut, posuere sed, felis. Vestibulum placerat feugiat nisl. Suspendisse lacinia, odio non feugiat vestibulum, sem erat blandit metus, ac nonummy magna odio pharetra felis. Vivamus vehicula velit non metus faucibus auctor. Nam sed augue. Donec orci. Cras eget diam et dolor dapibus sollicitudin. In lacinia, tellus vitae laoreet ultrices, lectus ligula dictum dui, eget condimentum velit dui vitae ante. Nulla nonummy augue nec pede. Pellentesque ut nulla. " +
                "Donec at libero. Pellentesque at nisl ac nisi fermentum viverra. Praesent odio. Phasellus tincidunt diam", new Date()));
        threads.add(new MessageThread(6, getUser().getProfilePicture(), "Palo Drapak", "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean et est a dui semper facilisis. Pellentesque placerat elit a nunc. Nullam tortor odio, rutrum quis, egestas ut, posuere sed, felis. Vestibulum placerat feugiat nisl. Suspendisse lacinia, odio non feugiat vestibulum, sem erat blandit metus, ac nonummy magna odio pharetra felis. Vivamus vehicula velit non metus faucibus auctor. Nam sed augue. Donec orci. Cras eget diam et dolor dapibus sollicitudin. In lacinia, tellus vitae laoreet ultrices, lectus ligula dictum dui, eget condimentum velit dui vitae ante. Nulla nonummy augue nec pede. Pellentesque ut nulla. " +
                "Donec at libero. Pellentesque at nisl ac nisi fermentum viverra. Praesent odio. Phasellus tincidunt diam", new Date()));

        return threads;
    }

    public static List<News> getNews(Context context) {
        List<News> news = new ArrayList<>();
        NewsContent c1 = new PartyPictures(context);//TODO null context
        news.add(new News(c1, new Date(), getUser()));
        news.add(new News(c1, new Date(), getUser()));
        news.add(new News(c1, new Date(), getUser()));
        news.add(new News(c1, new Date(), getUser()));
        news.add(new News(c1, new Date(), getUser()));
        news.add(new News(c1, new Date(), getUser()));
        news.add(new News(c1, new Date(), getUser()));

        return news;
    }

    public static class Properties{
        //cloud messaging...
        public static String SENT_TOKEN_TO_SERVER = "";
        public static String REGISTRATION_COMPLETE = "Cloud channel registration complete";
        //...cloud messaging

    }
}
