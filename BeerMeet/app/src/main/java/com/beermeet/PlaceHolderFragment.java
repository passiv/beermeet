package com.beermeet;

/**
 * Created by passiv on 07/03/16.
 */

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beermeet.ui.CircularImageView;
import com.facebook.appevents.AppEventsLogger;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceHolderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */

    static String text;
    static TextView textView;
    static int section=0;
    private static TabSelection tab;

    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceHolderFragment newInstance(int sectionNumber, TabSelection p_tab) {
        section=sectionNumber;
        tab=p_tab;

        PlaceHolderFragment fragment = new PlaceHolderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tabbed, container, false);

        textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setText(Cache.getUser().getFullName());

        CircularImageView profilePictureView = (CircularImageView)rootView.findViewById(R.id.profilePicture);
        Bitmap profilePicture = Cache.getUser().getProfilePicture();
        profilePictureView.setImageBitmap(profilePicture);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(getActivity());
    }

    public void setText(final String s) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.setText(s);
            }
        });
    }

    public TabSelection getTab() {
        return tab;
    }
}

