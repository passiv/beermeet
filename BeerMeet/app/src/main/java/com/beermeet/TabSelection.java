package com.beermeet;

/**
 * Created by passiv on 01/03/16.
 */
public enum TabSelection {
    HOME(R.string.home,0),
    SEARCH(R.string.search,1),
    FRIENDS(R.string.friends,2);

    int resourceId;
    int index;

    private TabSelection(int resourceId, int index){
        this.resourceId =resourceId;
        this.index=index;
    }

    public int getResourceId(){
        return resourceId;
    }

    public int getIndex(){
        return index;
    }
}
