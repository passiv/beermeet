package com.beermeet.facebook;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by passiv on 03/03/16.
 */
public class User implements Serializable{
    static DateFormat dateFormat = new SimpleDateFormat("MM/DD/yyyy");

    private String firstName, lastName, fullName;
    private Date birthday;
    private Gender gender;
    private Bitmap profilePicture;

    public User(){

    }

    public User(String firstName, String fullName, Date birthday, Gender gender, Bitmap profilePicture) {
        this.firstName = firstName;
        this.fullName = fullName;
        this.birthday = birthday;
        this.gender = gender;
        this.profilePicture=profilePicture;
    }

    public User setName(String firstName){
        this.firstName=firstName;
        return this;
    }

    public static User parseUserFromJSON(JSONObject jsonObject){
        try {
            String firstName=jsonObject.getString("first_name");
            String fullName=jsonObject.getString("name");
            Date birthday = createBirthDate(jsonObject);
            Gender gender=jsonObject.getString("gender").equalsIgnoreCase(Gender.MALE.name()) ? Gender.MALE : Gender.FEMALE;

            return new User(firstName,fullName,birthday,gender, createProfilePic(jsonObject));
        } catch (JSONException e) {
            Log.e("User.parseUserFromJson","Parsing user from JSON object failed");
            e.printStackTrace();
        } catch (ParseException p){
            Log.e("User.parseUserFromJson","Could not parse date! (wrong date format?)");
            p.printStackTrace();
        }
        return null;
    }

    private static Date createBirthDate(JSONObject jsonObject) throws ParseException, JSONException {
        if (jsonObject.has("birthday")) {
            return dateFormat.parse(jsonObject.getString("birthday"));
        }
        return null;
    }

    private static Bitmap createProfilePic(JSONObject jsonObject) throws JSONException {
        Bitmap profilePic = null;
        if (jsonObject.has("picture")) {
            String profilePicUrl = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");
            profilePic = getFacebookProfilePicture(profilePicUrl);
        }
        return profilePic;
    }

    public static Bitmap getFacebookProfilePicture(String url){
        URL facebookProfileURL= null;
        Bitmap bitmap = null;
        try {
            facebookProfileURL = new URL(url);
            bitmap = BitmapFactory.decodeStream(facebookProfileURL.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public Bitmap getProfilePicture(){
        return profilePicture;
    }
}
