package com.beermeet.facebook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;

import com.beermeet.LoginActivity;
import com.beermeet.TabbedActivity;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONObject;

/**
 * Created by passiv on 03/03/16.
 */
public class UserRequestTask extends AsyncTask<Void,Void,User>{

    User user;

    protected User getUser(){

        if(user==null) {
            AccessToken accessToken = LoginActivity.getAccessToken();
            Bundle params = new Bundle();
            params.putString("fields", "name,first_name,id,email,gender,cover,picture.type(large),birthday");

            new GraphRequest(accessToken, "me", params, HttpMethod.GET,
                    new GraphRequest.Callback() {
                        @Override
                        public void onCompleted(GraphResponse response) {
                            if (response != null) {
                                try {
                                    JSONObject data = response.getJSONObject();
                                    user = User.parseUserFromJSON(data);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).executeAndWait();
        }

        return user;
    }

    @Override
    protected User doInBackground(Void... params) {
        return getUser();
    }

}
