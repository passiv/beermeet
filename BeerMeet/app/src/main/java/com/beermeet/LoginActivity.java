package com.beermeet;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends AppCompatActivity {

    private static final int SETTINGS_INTENT_CODE = 9003;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private static AccessToken accessToken;
    private AccessTokenTracker accessTokenTracker;
    private AlertDialog errorDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume(){
        super.onResume();
        checkConnection();
    }

    private void checkConnection() {
        if(isNetworkAvailable()){
            createFacebookLogin();
        }else{
            errorDialog=createConnectionIssueErrorDialog();
            errorDialog.show();
        }
    }

    private void createFacebookLogin() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login);
        loginButton = (LoginButton)findViewById(R.id.login_button);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("fb login", "success");

                loginButton.setVisibility(View.INVISIBLE); //<- IMPORTANT

                Intent intent = new Intent(getApplicationContext(), TabbedActivity.class);
                startActivity(intent);
                finish();//<- IMPORTANT
            }

            @Override
            public void onCancel() {
                Log.d("fb login","cancel");
                Toast.makeText(getApplicationContext(), "fb login cancelled!", Toast.LENGTH_LONG);
            }

            @Override
            public void onError(FacebookException e) {
                Log.d("fb login","error");
                Toast.makeText(getApplicationContext(),"fb login error!", Toast.LENGTH_LONG);

            }
        });

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using§
                // currentAccessToken when it's loaded or set.
                if(oldAccessToken==null || !oldAccessToken.equals(currentAccessToken)) {
                    accessToken = currentAccessToken;
                }
            }
        };
        // If the access token is available already assign it.
        accessToken = AccessToken.getCurrentAccessToken();
        // If already logged in show the home view
        if (accessToken != null) {//<- IMPORTANT
            Intent intent = new Intent(getApplicationContext(), TabbedActivity.class);
            startActivity(intent);
            finish();//<- IMPORTANT
        }
    }

    private AlertDialog createConnectionIssueErrorDialog(){
        if(errorDialog==null) {
            return new AlertDialog.Builder(this)
                    .setTitle("Connection issue")
                    .setMessage("Please turn on internet connection to continue")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent settingsIntent = new Intent(Settings.ACTION_SETTINGS);

                            startActivityForResult(settingsIntent, SETTINGS_INTENT_CODE);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .create();
        }else{
            return errorDialog;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        hideErrorDialog();

        if(requestCode==SETTINGS_INTENT_CODE){
            checkConnection();
        }else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void hideErrorDialog() {
        if(errorDialog!=null){
            errorDialog.dismiss();
        }
    }

    public static AccessToken getAccessToken(){
        try{
            if(accessToken!=null){
                return accessToken;
            }else{
                throw new Exception("The access token is null !");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
