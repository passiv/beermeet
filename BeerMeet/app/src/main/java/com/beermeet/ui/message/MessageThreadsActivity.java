package com.beermeet.ui.message;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.beermeet.Cache;
import com.beermeet.R;

import java.util.List;

public class MessageThreadsActivity extends AppCompatActivity {

    ListView messageThreadsListView;
    MessageThreadsAdapter adapter;
    List<MessageThread> messageThreads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_threads);

        messageThreadsListView = getListView();
        messageThreads = Cache.getLatestMessageThreads();

        adapter=new MessageThreadsAdapter(this.getApplicationContext(), messageThreads);
        setListAdapter(adapter);
    }

    public void addItems(MessageThread thread) {
        messageThreads.add(thread);
        adapter.notifyDataSetChanged();
    }

    protected ListView getListView() {
        if (messageThreadsListView == null) {
            messageThreadsListView = (ListView) findViewById(R.id.message_threads_listview);

            messageThreadsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), MessageActivity.class);
                    intent.putExtra("MESSAGE_THREAD_ID", messageThreads.get(position).getId());
                    startActivity(intent);
                }
            });
        }
        return messageThreadsListView;
    }

    protected void setListAdapter(ListAdapter adapter) {
        getListView().setAdapter(adapter);
    }

    private class MessageThreadsAdapter extends ArrayAdapter<MessageThread> {
        private final Context context;
        private final List<MessageThread> values;

        public MessageThreadsAdapter(Context context, List<MessageThread> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.message_thread, parent, false);

            MessageThread currentThread = values.get(position);

            ImageView imageView = (ImageView) rowView.findViewById(R.id.message_thread_profilePicture);
            TextView header = (TextView) rowView.findViewById(R.id.message_thread_header);
            TextView insight = (TextView) rowView.findViewById(R.id.message_thread_insight);

            imageView.setImageBitmap(currentThread.getRecipientPhoto());
            header.setText(currentThread.getRecipientName());
            insight.setText(currentThread.getLastMessage());

            return rowView;
        }
    }

}
