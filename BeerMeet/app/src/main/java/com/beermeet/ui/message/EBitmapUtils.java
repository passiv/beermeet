package com.beermeet.ui.message;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;

/**
 * Created by passiv on 20/03/16.
 */
public class EBitmapUtils {

    public static Bitmap getResizedBitmap(Bitmap image, int newHeight, int newWidth) {
        int width = image.getWidth();
        int height = image.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(image, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;

    }



    public static BitmapDrawable getDrawable(Resources res, int imgRes
            , int width, int height){

        Bitmap bm = BitmapFactory.decodeResource(res, imgRes);
        Bitmap bmResized = getResizedBitmap(bm, height, width);
        bm.recycle();
        return new BitmapDrawable(res, bmResized);
    }

}
