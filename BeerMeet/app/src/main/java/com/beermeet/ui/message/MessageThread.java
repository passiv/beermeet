package com.beermeet.ui.message;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Entity to store whom the current user is having conversations with
 *  - recipient name + photo
 *  - last message
 *
 *  TODO : support for multiple users conversation
 */

public class MessageThread  implements Comparator{

    Integer id;
    Bitmap recipientPhoto;
    String recipientName;
    String lastMessage;
    Date lastMessageDate;

    public MessageThread(Integer id, Bitmap recipientPhoto, String recipientName, String lastMessage, Date lastMessageDate){
        this.id=id;
        this.recipientPhoto=recipientPhoto;
        this.recipientName=recipientName;
        this.lastMessage=lastMessage;
        this.lastMessageDate=lastMessageDate;
    }

    public List<Message> getMessages(){//TODO load messages from db
        List<Message> messages = new ArrayList<>();
        messages.add(new Message("cau", new Date(), true));
        messages.add(new Message("hello", new Date(), false));
        messages.add(new Message("jebe ti?", new Date(), true));
        messages.add(new Message("The Tinkoff team made their intentions apparent right from the start by putting both Robert Kiserlovski and Yuri Trofimov into the breakaway. When Contador made his first attack on the Cote de Peille, he was able to rely on the help of his two teammates. The plan didn’t quite come off, and he had to fight off his emotion at missing out while on the podium. The team, however, is trying to look on the bright side of things."
                , new Date(), false));
        messages.add(new Message("random text"
                , new Date(), false));

        for(int i=0;i<100;i++){
            messages.add(new Message("The Tinkoff team made their intentions apparent right from the start by putting both Robert Kiserlovski and Yuri Trofimov into the breakaway. When Contador made his first attack on the Cote de Peille, he was able to rely on the help of his two teammates. The plan didn’t quite come off, and he had to fight off his emotion at missing out while on the podium. The team, however, is trying to look on the bright side of things."
                    , new Date(), false));
        }
        return messages;
    }

    @Override
    public int compare(Object lhs, Object rhs) {
        MessageThread m1 = (MessageThread) lhs;
        MessageThread m2 = (MessageThread) rhs;

        if(m1.lastMessageDate.after(m2.lastMessageDate)){
            return 1;
        }
        if(m1.lastMessageDate.before(m2.lastMessageDate)){
            return -1;
        }
        return 0;
    }

    public Integer getId(){
        return id;
    }

    public void changeLatestMessage(String message){
        this.lastMessage = message;
    }

    public Bitmap getRecipientPhoto() {
        return recipientPhoto;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public Date getLastMessageDate() {
        return lastMessageDate;
    }
}
