package com.beermeet.ui.message;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.beermeet.Cache;
import com.beermeet.R;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageActivity extends AppCompatActivity {

    ImageView recipientPhoto;
    TextView recipientName;

    ListView messages;
    Integer messageThreadId=-1;
    MessagesAdapter adapter;

    EditText inputText;
    ImageButton sendButton;

    ScrollView backgroundScroll;
    private ListViewScrollTracker mScrollTracker;
    private int mListViewScrollY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_message);

        setBackgroundResource(R.drawable.black_table_background);

        recipientPhoto=(ImageView) findViewById(R.id.message_recipient_picture);
        recipientName=(TextView) findViewById(R.id.message_recipient_name);

        recipientPhoto.setImageBitmap(Cache.getUser().getProfilePicture());
        recipientName.setText(Cache.getUser().getFirstName());

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            messageThreadId = extras.getInt("MESSAGE_THREAD_ID");
        }

        messages= getMessagesListView();
        adapter=new MessagesAdapter(this.getApplicationContext(), loadMessages(messageThreadId));

        messages.setAdapter(adapter);
        messages.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        messages.setStackFromBottom(true);

        refreshListPosition();

        inputText = (EditText)findViewById(R.id.messageText);
        inputText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                refreshListPosition();
            }
        });
        sendButton = (ImageButton)findViewById(R.id.sendMessage);

        backgroundScroll = (ScrollView)findViewById(R.id.act_background_scrollview);

        mScrollTracker = new ListViewScrollTracker(messages);
        messages.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                int incrementalOffset = mScrollTracker.calculateIncrementalOffset(firstVisibleItem, visibleItemCount);
                View c = getMessagesListView().getChildAt(0);
                int slowness=3;

                if(c!=null){
                    int count = messages.getCount();
                    slowness = count/4;
                }

                mListViewScrollY-=incrementalOffset/slowness; // slowness is sort of inverted scroll speed (the bigger the slowness, the slower the background moves)
                backgroundScroll.scrollTo(0, mListViewScrollY); //background scrolls 3x slower than actual listview

            }
        });

    }

    private void refreshListPosition() {
        messages.setSelection(adapter.getCount() - 1);
    }

    public List<Message> loadMessages(Integer messageThreadId){ //TODO load from db
        MessageThread dummy = new MessageThread(-1,Cache.getUser().getProfilePicture(),"Palo Kabela","last message created in MessageActivity", new Date());
        List<Message> messages = dummy.getMessages();
        return messages;
    }

    public void sendMessage(View view){
        //TODO send message. Current code only adds message to UI
        String messageText = inputText.getText().toString();
        if(!messageText.isEmpty()) {
            adapter.add(new Message(messageText,new Date(),true));
            inputText.setText("");
            refreshListPosition();
        }
    }

    public ListView getMessagesListView(){//TODO  load from cache(or local db) + load messages newer then newest message from cache
        if(messages==null){
            messages = (ListView) findViewById(R.id.message_messages);

            messages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //go through all other messages and hide date

                    for(int i=0;i<messages.getCount();i++){
                        View message = messages.getChildAt(i);
                        if(message!=null) {
                            View otherDate = message.findViewById(R.id.message_sent_date);
                            otherDate.setVisibility(View.GONE);
                        }
                    }

                    View date = view.findViewById(R.id.message_sent_date);
                    displayComponentAnimation(date);
                    messages.setSelection(position);
                }
            });

        }
        return messages;
    }

    public static Map<Integer, BitmapDrawable> imageMap = new HashMap<Integer, BitmapDrawable>();
    protected void setBackgroundResource(int imgResourceId) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = (int)(size.x * 2);
        int height = (int)(size.y * 2);

        mListViewScrollY = size.y;

        BitmapDrawable dr = imageMap.get(imgResourceId);
        if (dr == null) {
            dr = EBitmapUtils.getDrawable(getResources(), imgResourceId, width, height);
            imageMap.put(imgResourceId, dr);
        }


        ((ImageView)findViewById(R.id.message_background_wallpaper))
                .setImageDrawable(dr);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void displayComponentAnimation(View myView) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            myView.setVisibility(View.VISIBLE);
        }else {
            int cx = myView.getWidth() / 2;
            int cy = myView.getHeight() / 2;
            float finalRadius = (float) Math.hypot(cx, cy);
            Animator anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
            myView.setVisibility(View.VISIBLE);
            anim.start();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void hideComponentAnimation(final View myView) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            myView.setVisibility(View.GONE);
        } else {
            int cx = myView.getWidth() / 2;
            int cy = myView.getHeight() / 2;
            float initialRadius = (float) Math.hypot(cx, cy);

            Animator anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);

            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    myView.setVisibility(View.GONE);
                }
            });
            anim.start();
        }
    }

    private class MessagesAdapter extends ArrayAdapter<Message> {
        private final Context context;
        private final List<Message> values;

        public MessagesAdapter(Context context, List<Message> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            Message currentMessage = values.get(position);
            View rowView;
            if(currentMessage.isSentByMe()){
                rowView = inflater.inflate(R.layout.message_from_me, parent, false);
            }else{
                rowView = inflater.inflate(R.layout.message_for_me, parent, false);
            }
            TextView messageText = (TextView) rowView.findViewById(R.id.messageText);
            messageText.setText(currentMessage.getText());

        return rowView;
        }
    }
}
