package com.beermeet.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.beermeet.Cache;
import com.beermeet.R;
import com.beermeet.ui.message.Message;

import java.util.List;

public class HomeFragment extends Fragment {

    ListView newsList;
    NewsAdapter newsAdapter;

    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        newsList = (ListView) getActivity().findViewById(R.id.news_feed_listView);
        newsAdapter = new NewsAdapter(getContext(), Cache.getNews(getContext()));
        newsList.setDivider(null);
        newsList.setAdapter(newsAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    private class NewsAdapter extends ArrayAdapter<News> {
        private final Context context;
        private final List<News> news;

        public NewsAdapter(Context context, List<News> news) {
            super(context, -1, news);
            this.context = context;
            this.news = news;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            NewsContent current = news.get(position).getContent();
            View rowView;
            rowView = current.getNewsContent();
            return rowView;
        }
    }

}
