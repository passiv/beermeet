package com.beermeet.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.beermeet.Cache;
import com.beermeet.R;
import com.beermeet.ui.CircularImageView;

/**
 * Created by passiv on 15/03/16.
 */
public class PartyPictures extends View implements NewsContent {

    public PartyPictures(Context context) {
        super(context);
    }

    @Override
    public View getNewsContent() {
        View view = LayoutInflater.from(super.getContext()).inflate(R.layout.news_party_pictures, null);
        CircularImageView profilePicture = (CircularImageView) view.findViewById(R.id.news_feed_profilePicture);
        profilePicture.setImageBitmap(Cache.getUser().getProfilePicture());
        return view;
    }
}
