package com.beermeet.ui.home;

import android.view.View;

import com.beermeet.facebook.User;

import java.util.Date;

/**
 * Created by passiv on 15/03/16.
 */
public class News {

    User origin; //who invoked the news
    Date date;
    NewsContent content;

    public News(NewsContent content, Date date, User origin) {
        this.content = content;
        this.date = date;
        this.origin = origin;
    }

    public User getOrigin() {
        return origin;
    }

    public Date getDate() {
        return date;
    }

    public NewsContent getContent() {
        return content;
    }

    public View getContentView(){
        return content.getNewsContent();
    }
}
