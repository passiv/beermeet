package com.beermeet.ui.message;

import java.util.Date;

/**
 * Created by passiv on 13/03/16.
 */
public class Message {

    String text;
    Date sent;
    boolean sentByMe;

    public Message(String text, Date sent, boolean sentByMe){
        this.text=text;
        this.sent=sent;
        this.sentByMe=sentByMe;
    }

    public String getText() {
        return text;
    }

    public Date getSent() {
        return sent;
    }

    public boolean isSentByMe() {
        return sentByMe;
    }
}
