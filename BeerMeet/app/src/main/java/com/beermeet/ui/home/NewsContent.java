package com.beermeet.ui.home;

import android.view.View;

/**
 * Created by passiv on 15/03/16.
 */
public interface NewsContent {
    View getNewsContent();
}
