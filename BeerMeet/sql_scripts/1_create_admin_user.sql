CREATE TABLESPACE closer_app_tablespace
  DATAFILE 'closer_app_tablespace.dat' 
    SIZE 10M
    REUSE
    AUTOEXTEND ON NEXT 10M MAXSIZE 1000M;


CREATE PROFILE admin_profile LIMIT 
   SESSIONS_PER_USER          UNLIMITED 
   CPU_PER_SESSION            UNLIMITED 
   CPU_PER_CALL               3000 
   CONNECT_TIME               45 
   LOGICAL_READS_PER_SESSION  DEFAULT 
   LOGICAL_READS_PER_CALL     1000 
   PRIVATE_SGA                15K
   COMPOSITE_LIMIT            5000000; 

CREATE USER closer_db_user 
    IDENTIFIED BY Closer786 
    DEFAULT TABLESPACE closer_app_tablespace 
    QUOTA 10M ON closer_app_tablespace 
    TEMPORARY TABLESPACE temp
    QUOTA 5M ON system 
    PROFILE admin_profile;
    
grant connect to closer_db_user;
grant all privileges to closer_db_user;

commit;
