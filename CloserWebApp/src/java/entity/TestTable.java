/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author passiv
 */
@Entity
@Table(name = "TEST_TABLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TestTable.findAll", query = "SELECT t FROM TestTable t"),
    @NamedQuery(name = "TestTable.findByContent", query = "SELECT t FROM TestTable t WHERE t.content = :content")})
public class TestTable implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "CONTENT")
    private String content;

    public TestTable() {
    }

    public TestTable(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (content != null ? content.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TestTable)) {
            return false;
        }
        TestTable other = (TestTable) object;
        if ((this.content == null && other.content != null) || (this.content != null && !this.content.equals(other.content))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.TestTable[ content=" + content + " ]";
    }
    
}
